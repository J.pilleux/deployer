all: build

GO_FOLDERS := $(shell go list ./...)

OUTPUT := ./bin/deployer
MAIN_FILE := ./cmd/deployer

build:
	go build -o $(OUTPUT) $(MAIN_FILE)

format:
	go fmt $(GO_FOLDERS)

test:
	ginkgo run -r

vet:
	go vet $(GO_FOLDERS)

lint:
	golangci-lint run

debug: build
	./bin/deployer -v -f ./resources/sample_conf.yaml

conf: build
	./bin/deployer -c -f ./resources/sample_conf.yaml

spellcheck:
	cspell /**/*.go

golangci:
	golangci-lint run

prepush: build format test vet golangci

clean:
	go clean
	rm -rf bin
