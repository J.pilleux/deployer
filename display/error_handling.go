package display

import (
	"fmt"
	"os"
)

func FatalHandling(msg string, err error) {
	if err != nil {
		errorMsg := fmt.Sprintf("%s : %s", msg, err.Error())
		PrintErr(errorMsg)
		os.Exit(1)
	}
}

func DisplayError(msg string, err error) {
	if err != nil {
		errorMsg := fmt.Sprintf("%s : %s", msg, err.Error())
		PrintErr(errorMsg)
	}
}

func ErrorCallback(msg string, err error, callback func()) {
	if err != nil {
		errorMsg := fmt.Sprintf("%s : %s", msg, err.Error())
		PrintErr(errorMsg)
		callback()
	}
}
