package display

import "fmt"

const (
	Red    = "\033[31m%s\033[0m"
	Green  = "\033[32m%s\033[0m"
	Yellow = "\033[33m%s\033[0m"
	Blue   = "\033[34m%s\033[0m"
)

func toRed(text string) string {
	return fmt.Sprintf(Red, text)
}

func toGreen(text string) string {
	return fmt.Sprintf(Green, text)
}

func toBlue(text string) string {
	return fmt.Sprintf(Blue, text)
}

func toYellow(text string) string {
	return fmt.Sprintf(Yellow, text)
}
