package display

import (
	"bytes"
	"fmt"
	"strings"

	"github.com/elliotchance/orderedmap/v2"
)

const (
	FullLen  = 80
	SideLeft = 10
	SideChar = "-"
)

func FormatHeaderName(headerName string) string {
	nameLen := len(headerName) // Taking two to take two spaces on both side
	restLen := FullLen - ((SideLeft + 1) + nameLen)
	return fmt.Sprintf("%s %s %s", toYellow(strings.Repeat(SideChar, SideLeft)), toBlue(headerName), toYellow(strings.Repeat(SideChar, restLen)))
}

func SuccessMessage(message string) string {
	return fmt.Sprintf("%s : %s", toGreen("SUCCESS"), message)
}

func ErrorMessage(message string) string {
	return fmt.Sprintf("%s : %s\n", toRed("ERROR"), message)
}

func FormatSection(actionName string, actionMessage string) string {
	header := FormatHeaderName(actionName)
	return fmt.Sprintf("%s\n\n%s", header, actionMessage)
}

func FormatError(errorMessage string, err error) string {
	return fmt.Sprintf("%s\n%s", errorMessage, ErrorMessage(err.Error()))
}

func getMaxStrLength(words []string) int {
	maxLength := 0
	for _, key := range words {
		l := len(key)
		if l > maxLength {
			maxLength = l
		}
	}
	return maxLength
}

func TabEachLine(lines *orderedmap.OrderedMap[string, string]) string {
	var buffer bytes.Buffer
	var maxLen = getMaxStrLength(lines.Keys())

	for _, key := range lines.Keys() {
		value, ok := lines.Get(key)
		if !ok {
			continue // Ignoring unknown keys
		}
		buffer.WriteString(fmt.Sprintf("\t%-*s : %s\n", maxLen, key, value))
	}

	return buffer.String()
}

func ExpectedMessage(what, expected, actual string) string {
	return fmt.Sprintf("%s incorrect: Expected=%s, Got=%s", what, expected, actual)
}
