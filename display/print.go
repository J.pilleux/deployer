package display

import (
	"fmt"
	"os"
)

func PrintErr(msg string) {
	fmt.Fprint(os.Stderr, ErrorMessage(msg))
}
