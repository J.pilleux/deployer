module deployer

go 1.18

require golang.org/x/crypto v0.0.0-20220507011949-2cf3adece122

require (
	github.com/elliotchance/orderedmap/v2 v2.0.1 // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-task/slim-sprig v0.0.0-20210107165309-348f09dbbbc0 // indirect
	github.com/google/pprof v0.0.0-20210407192527-94a9f03dee38 // indirect
	github.com/nxadm/tail v1.4.8 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/ginkgo/v2 v2.1.4 // indirect
	github.com/onsi/gomega v1.19.0 // indirect
	github.com/zenizh/go-capturer v0.0.0-20211219060012-52ea6c8fed04 // indirect
	golang.org/x/exp v0.0.0-20220321173239-a90fa8a75705 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.10 // indirect
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

require (
	github.com/bramvdbogaerde/go-scp v1.2.0
	github.com/elliotchance/orderedmap v1.4.0
	golang.org/x/sys v0.0.0-20220319134239-a9b59b0215f8 // indirect
	gopkg.in/yaml.v3 v3.0.0-20220512140231-539c8e751b99
)
