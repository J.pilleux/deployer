package cli

import (
	"deployer/display"
	"flag"
	"fmt"
	"os"
)

const (
	FlagNotSet string = "__FLAG_NOT_SET__"

	FilePathFlag string = "f"
	VerboseFlag  string = "v"
	AddressFlag  string = "a"
	ConfigFlag   string = "c"

	OptionalAddress string = "Address"
)

type ProgramFlags struct {
	ConfigFilePath     string
	ConfigPrintFlag    bool
	optionalParameters map[string]string
}

func newProgramFlags(configFilePath string, configFlag bool) ProgramFlags {
	return ProgramFlags{
		ConfigFilePath:     configFilePath,
		ConfigPrintFlag:    configFlag,
		optionalParameters: map[string]string{},
	}
}

func (f *ProgramFlags) addOptionalFlag(name string, value string) {
	if name != FlagNotSet {
		f.optionalParameters[name] = value
	}
}

func (f *ProgramFlags) GetOptionalFlag(name string) (string, error) {
	value, ok := f.optionalParameters[name]
	if !ok {
		return "", fmt.Errorf("the flag '%s' is not found", name)
	}
	return value, nil
}

func parseFlags() (*ProgramFlags, error) {
	verboseFlag := flag.Bool(VerboseFlag, false, "[Optional] Verbose mode. Will print actions")
	configFlag := flag.Bool(ConfigFlag, false, "Print the configuration then exit")
	fileFlag := flag.String(FilePathFlag, FlagNotSet, "The YAML configuration file")
	addressFlag := flag.String(AddressFlag, FlagNotSet, "[Optional] overrides the address in configuration")

	flag.Parse()

	progFlags := newProgramFlags(*fileFlag, *configFlag)
	progFlags.addOptionalFlag(OptionalAddress, *addressFlag)

	if *verboseFlag {
		SetEnvVariable(EnvIsVerboseVar, EnvTrue)
	}

	return &progFlags, nil
}

func GetProgramFlags() *ProgramFlags {
	flags, err := parseFlags()

	if err != nil {
		display.PrintErr(err.Error())
		flag.Usage()
		os.Exit(1)
	}

	return flags
}
