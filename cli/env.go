package cli

import (
	"deployer/display"
	"fmt"
	"os"
)

const (
	EnvTrue  = "1"
	EnvFalse = "0"

	EnvIsVerboseVar = "_DEPLOYER_IS_VERBOSE"
)

func SetEnvVariable(name string, value string) {
	err := os.Setenv(name, value)
	display.FatalHandling(fmt.Sprintf("Cannot set variable '%s' with value '%s'", name, value), err)
}

func PrintIfVerbose(msg string) {
	if EnvIsVerbose() {
		fmt.Printf("%s\n", msg)
	}
}

func EnvIsVerbose() bool {
	return os.Getenv(EnvIsVerboseVar) == EnvTrue
}
