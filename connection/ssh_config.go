package connection

import (
	"deployer/display"
	"fmt"
	"strconv"

	"github.com/elliotchance/orderedmap/v2"
)

type SSHConfig struct {
	Address  string
	Port     int
	Username string
}

func (conf *SSHConfig) GetAddress() string {
	return conf.Address
}

func (conf *SSHConfig) GetPort() int {
	return conf.Port
}

func (conf *SSHConfig) GetFullAddress() string {
	return fmt.Sprintf("%s:%d", conf.Address, conf.Port)
}

func (conf *SSHConfig) String() string {
	orderedMap := orderedmap.NewOrderedMap[string, string]()
	orderedMap.Set("Address", conf.Address)
	orderedMap.Set("Port", strconv.Itoa(conf.Port))
	orderedMap.Set("Username", conf.Username)
	return display.TabEachLine(orderedMap)
}

func (conf *SSHConfig) Equals(ssh SSHConfig) bool {
	return conf.Address == ssh.Address && conf.Port == ssh.Port
}
