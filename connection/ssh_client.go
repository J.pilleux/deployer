package connection

import (
	"bytes"
	"deployer/display"
	"fmt"
	"io"
	"os"
	"strings"
	"sync"

	"golang.org/x/crypto/ssh"
)

const BoolShellTrue string = "0"
const BoolShellFalse string = "1"

const (
	Folder = "-d"
	File   = "-f"
)

type SSHClient struct {
	Config *ssh.ClientConfig
	Host   string
	Port   int
}

func (client *SSHClient) RunCommand(cmd *SSHCommand) (string, error) {
	var (
		session *ssh.Session
		err     error
		buf     bytes.Buffer
	)

	cmd.Stdout = &buf
	cmd.Stderr = &buf

	if session, err = client.newSession(); err != nil {
		return "", err
	}

	defer func() {
		err := session.Close()
		if err.Error() != "EOF" {
			display.DisplayError("Cannot close session while running a command", err)
		}
	}()

	if err = client.prepareCommand(session, cmd); err != nil {
		return "", err
	}

	err = session.Run(cmd.Path)
	return strings.TrimSpace(buf.String()), err
}

func copyWriter(wg *sync.WaitGroup, output io.Reader, cmdWriter io.Writer) {
	defer wg.Done()
	_, err := io.Copy(cmdWriter, output)
	display.DisplayError("io copy error : ", err)
}

func (client *SSHClient) prepareCommand(session *ssh.Session, cmd *SSHCommand) error {
	if cmd.Stdout != nil {
		stdout, err := session.StdoutPipe()
		if err != nil {
			return err
		}
		cmd.WaitGroup.Add(1)
		go copyWriter(&cmd.WaitGroup, stdout, cmd.Stdout)
	}

	if cmd.Stdout != nil {
		stderr, err := session.StderrPipe()
		if err != nil {
			return err
		}
		cmd.WaitGroup.Add(1)
		go copyWriter(&cmd.WaitGroup, stderr, cmd.Stdout)
	}

	return nil
}

func (client *SSHClient) newSession() (*ssh.Session, error) {
	connection, err := ssh.Dial("tcp", fmt.Sprintf("%s:%d", client.Host, client.Port), client.Config)
	display.FatalHandling("Failed to dial", err)

	session, err := connection.NewSession()
	if err != nil {
		return nil, err
	}

	modes := ssh.TerminalModes{
		ssh.TTY_OP_ISPEED: 14400,
		ssh.TTY_OP_OSPEED: 14400,
	}

	err = session.RequestPty("xterm", 80, 40, modes)
	display.ErrorCallback("Request for pseudo terminal failed", err, func() {
		err = session.Close()
		if err.Error() != "EOF" {
			display.FatalHandling("Cannot close session when creating a new session", err)
		}
	})

	return session, nil
}

func (client *SSHClient) Run(command *SSHCommand) (string, error) {
	return client.RunCommand(command)
}

func (client *SSHClient) CheckExists(path string, what string) bool {
	if what != File && what != Folder {
		fmt.Fprintf(os.Stderr, "I cannot search for %s", what)
		return false
	}

	testCommand := fmt.Sprintf("[[ %s %s ]] && echo %s || echo %s", what, path, BoolShellTrue, BoolShellFalse)

	cmd := NewCommand(testCommand)

	commandOutput, err := client.RunCommand(cmd)
	if err != nil {
		return false
	}
	isFilePresent := false
	if commandOutput == BoolShellTrue {
		isFilePresent = true
	}

	return isFilePresent
}
