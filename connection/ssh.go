package connection

import (
	"deployer/display"
	"fmt"
	"io/ioutil"
	"os/user"

	"golang.org/x/crypto/ssh"
)

func CreateSSHConfig(username string) (*ssh.ClientConfig, error) {
	key, err := getKeyFile()
	if err != nil {
		return nil, err
	}

	return &ssh.ClientConfig{
		User: username,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(key),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(), //nolint
	}, nil
}

func getKeyFile() (key ssh.Signer, err error) {
	usr, err := user.Current()
	display.FatalHandling("Cannot get current user", err)

	file := usr.HomeDir + "/.ssh/id_rsa"
	buf, err := ioutil.ReadFile(file)
	display.FatalHandling(fmt.Sprintf("Cannot read private key '%s'", file), err)

	key, err = ssh.ParsePrivateKey(buf)
	display.FatalHandling(fmt.Sprintf("Cannot parse private key '%s'", file), err)
	return
}

func CreateSSHClient(conf SSHConfig) (*SSHClient, error) {
	sshConfig, err := CreateSSHConfig(conf.Username)
	if err != nil {
		return nil, fmt.Errorf("Create SSH config : %s", err.Error())
	}

	return &SSHClient{
		Config: sshConfig,
		Host:   conf.Address,
		Port:   conf.Port,
	}, nil
}
