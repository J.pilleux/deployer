package connection

import (
	"io"
	"os"
	"sync"
)

type SSHCommand struct {
	Path      string
	Stdout    io.Writer
	Stderr    io.Writer
	WaitGroup sync.WaitGroup
}

func NewCommand(command string) *SSHCommand {
	return &SSHCommand{
		Path:      command,
		Stdout:    os.Stdout,
		Stderr:    os.Stderr,
		WaitGroup: sync.WaitGroup{},
	}
}
