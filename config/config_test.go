package config

import (
	"deployer/display"
	"testing"
)

const content string = `
ssh:
    address: host_name
    port: 69
    username: Billy

distro: Debian

actions:
    - type: copy
      src: /tmp/source.file
      dst: /home/destination/source.file

    - type: git
      repositoryURL: ssh://git@gitlab.com/J.pilleux/deployer.git
      destinationFolder: /etc/deploy/directory

    - type: shell
      command: ls -a
`

func TestConfigParseConfiguration(t *testing.T) {
	conf, err := createFileConfigFromBytes([]byte(content))
	if err != nil {
		t.Errorf("The configuration reading should not fail: %v", err)
	}

	if conf.SSH.Address != "host_name" {
		t.Error(display.ExpectedMessage("SSH hostname", "host_name", conf.SSH.Address))
	}

	if conf.SSH.Port != 69 {
		t.Error(display.ExpectedMessage("SSH port", "69", string(conf.SSH.Address)))
	}

	if conf.SSH.Username != "Billy" {
		t.Error(display.ExpectedMessage("SSH user name", "Billy", conf.SSH.Username))
	}

	if conf.Distro != "Debian" {
		t.Error(display.ExpectedMessage("Distribution", "Debian", conf.Distro))
	}

	if len(conf.Actions) != 3 {
		t.Error(display.ExpectedMessage("actions length", "3", string(rune(len(conf.Actions)))))
	}
}
