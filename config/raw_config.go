package config

import (
	action "deployer/actions"
	"deployer/connection"
	"reflect"
)

type RawConfiguration struct {
	Distro  string
	SSH     connection.SSHConfig
	Actions []action.RawAction
}

func (c *RawConfiguration) Equals(conf RawConfiguration) bool {
	return c.SSH.Equals(conf.SSH) && reflect.DeepEqual(c.Actions, conf.Actions) && c.Distro == conf.Distro
}
