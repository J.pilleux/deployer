package config

import (
	"bytes"
	action "deployer/actions"
	"deployer/connection"
	"fmt"
)

type Configuration struct {
	ssh     connection.SSHConfig
	distro  string
	actions []action.Actioner
}

func (conf *Configuration) GetSSHConfig() connection.SSHConfig {
	return conf.ssh
}

func (conf *Configuration) GetActions() []action.Actioner {
	return conf.actions
}

func (conf *Configuration) getDistroString() string {
	if conf.distro == "" {
		return "No distro"
	}
	return conf.distro
}

func (conf *Configuration) String() string {
	var buffer bytes.Buffer
	buffer.WriteString(fmt.Sprintf("Target distribution : %s\n\n", conf.getDistroString()))

	buffer.WriteString("[ SSH configuration ]\n")
	buffer.WriteString(fmt.Sprintf("%s\n\n", conf.ssh.String()))

	buffer.WriteString("[ Action configuration ]\n")

	buffer.WriteString(getAllActionsRepresentation(conf.actions))

	return buffer.String()
}

func getAllActionsRepresentation(actions []action.Actioner) string {
	var buffer bytes.Buffer

	nbActions := len(actions)

	// Yeah, channel and go routines are overkill here, but I wanted to try these :)
	actionChannel := make(chan string, nbActions)

	if nbActions > 0 {
		for _, action := range actions {
			go askActionToString(actionChannel, action)
		}
	}

	for i := 0; i < nbActions; i++ {
		actionString, ok := <-actionChannel
		if !ok {
			return buffer.String()
		}
		buffer.WriteString(actionString)
	}

	return buffer.String()
}

func askActionToString(c chan string, action action.Actioner) {
	c <- fmt.Sprintf("%s\n", action.String())
}
