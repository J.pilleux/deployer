package config

import (
	action "deployer/actions"
	"deployer/cli"
	"deployer/display"
	"fmt"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

func readConfigurationFile(filePath string) ([]byte, error) {
	bytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	return bytes, nil
}

func createFileConfigFromBytes(fileContent []byte) (*RawConfiguration, error) {
	rawConf := RawConfiguration{}
	if err := yaml.Unmarshal(fileContent, &rawConf); err != nil {
		return nil, err
	}

	return &rawConf, nil
}

func createConfigurationFromRaw(configFilePath string, rawConfig *RawConfiguration) (*Configuration, error) {
	actions, err := action.CreateActionsFromRaw(rawConfig.Actions)
	if err != nil {
		return nil, fmt.Errorf("cannot parse actions from configuration file '%s' : %s", configFilePath, err.Error())
	}

	conf := &Configuration{
		ssh:     rawConfig.SSH,
		actions: actions,
	}

	return conf, nil
}

func loadConfigFromFile(configFilePath string) (*Configuration, error) {
	fileContent, err := readConfigurationFile(configFilePath)
	if err != nil {
		return nil, err
	}

	fileConf, err := createFileConfigFromBytes(fileContent)
	if err != nil {
		return nil, err
	}

	return createConfigurationFromRaw(configFilePath, fileConf)
}

func GetConfig() (*Configuration, error) {
	programFlags := cli.GetProgramFlags()

	cli.PrintIfVerbose(fmt.Sprintf("Loading configuration from file : %s\n", programFlags.ConfigFilePath))
	conf, err := loadConfigFromFile(programFlags.ConfigFilePath)
	if err != nil {
		return nil, err
	}

	if programFlags.ConfigPrintFlag {
		fmt.Printf("%s\n", display.FormatSection("Current configuration", conf.String()))
		os.Exit(0)
	}

	cli.PrintIfVerbose(display.FormatSection("Current configuration", conf.String()))

	return conf, nil
}
