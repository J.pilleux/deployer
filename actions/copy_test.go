package actions

import "testing"

func createRawCopy(src, dst, name string) RawAction {
	return RawAction{
		rawKeyName:    name,
		rawKeyType:    CopyType,
		rawKeyCopySrc: src,
		rawKeyCopyDst: dst,
	}
}

func TestCopyInitializationSuccess(t *testing.T) {
	raw := createRawCopy("/tmp", "/destination/folder", "Copy Folder")
	_, err := NewCopy(raw)

	if err != nil {
		t.Errorf("The initialization should be valid: %s", err)
	}
}

func TestCopyInitializationFail(t *testing.T) {
	raw := RawAction{
		"nil": "nil",
	}
	_, err := NewCopy(raw)
	if err == nil {
		t.Error("The initialization should not be valid without mandatory keys")
	}
}

func TestCopyInitializationFailWrongPath(t *testing.T) {
	src := "/source/wrong"
	raw := createRawCopy(src, "/destination/folder", "Copy Folder")
	_, err := NewCopy(raw)

	if err == nil {
		t.Errorf("The initialization should not be valid because of the source path: '%s'", src)
	}
}
