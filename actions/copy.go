package actions

import (
	"context"
	"deployer/connection"
	"deployer/display"
	"errors"
	"fmt"
	"os"

	"github.com/bramvdbogaerde/go-scp"
	"github.com/elliotchance/orderedmap/v2"
)

const (
	CopyType string = "copy"

	rawKeyCopySrc string = "src"
	rawKeyCopyDst string = "dst"
)

type CopyAction struct {
	Action

	src string
	dst string
}

func NewCopy(data RawAction) (*CopyAction, error) {
	mandatoryKeys := []string{rawKeyCopySrc, rawKeyCopyDst}

	err := data.CheckKeyAreIn(CopyType, mandatoryKeys)
	if err != nil {
		return nil, err
	}

	src := data[rawKeyCopySrc]
	if _, err = os.Stat(src); errors.Is(err, os.ErrNotExist) {
		return nil, fmt.Errorf("the file or directory '%s' does not exists", src)
	}

	a := &CopyAction{
		src:    src,
		dst:    data[rawKeyCopyDst],
		Action: *New(data.GetDefaultName("Copy Action")),
	}

	return a, nil
}

func (a *CopyAction) Exec(conf connection.SSHConfig) (string, error) {
	sshConfig, err := connection.CreateSSHConfig(conf.Username)
	if err != nil {
		return "", fmt.Errorf("SSHConfig creation : %s", err.Error())
	}

	scpClient := scp.NewClient(conf.GetFullAddress(), sshConfig)

	err = scpClient.Connect()
	if err != nil {
		return "", fmt.Errorf("SCP client connection : %s", err.Error())
	}

	fileData, err := os.Open(a.src)
	if err != nil {
		return "", fmt.Errorf("open private key : %s", err.Error())
	}

	err = scpClient.CopyFile(context.Background(), fileData, a.dst, "0655")
	if err != nil {
		return "", fmt.Errorf("copy file '%s' to '%s' : %s", a.src, a.dst, err.Error())
	}

	err = scpClient.Session.Close()
	if err != nil && err.Error() != "EOF" {
		return "", fmt.Errorf("close the SCP session : %s", err)
	}

	err = fileData.Close()
	if err != nil {
		return "", fmt.Errorf("close file '%s' : %s", a.src, err.Error())
	}

	return display.SuccessMessage(fmt.Sprintf("File copied from '%s' to '%s'", a.src, a.dst)), nil
}

func (a *CopyAction) String() string {
	orderedMap := orderedmap.NewOrderedMap[string, string]()
	orderedMap.Set("Type", CopyType)
	orderedMap.Set("Name", a.name)
	orderedMap.Set("Source", a.src)
	orderedMap.Set("Destination", a.dst)
	return display.TabEachLine(orderedMap)
}
