package actions

import (
	"testing"
)

func TestFactoryCreateCopy(t *testing.T) {
	raw := createRawCopy("/tmp", "source", "Copy")
	a, err := Factory(raw)
	if err != nil {
		t.Errorf("the copy initialization should not fail: %v", err)
	}

	if _, ok := a.(*CopyAction); !ok {
		t.Error("the struct should be of type 'CopyAction'")
	}
}

func TestFactoryCreateGit(t *testing.T) {
	raw := createRawGit("https://repo/git", "/tmp", "Git")
	a, err := Factory(raw)
	if err != nil {
		t.Errorf("the git initialization should not fail: %v", err)
	}

	if _, ok := a.(*GitAction); !ok {
		t.Error("the struct should be of type 'GitAction'")
	}
}

func TestFactoryCreateShell(t *testing.T) {
	raw := createRawShell("apt update", "Shell")
	a, err := Factory(raw)
	if err != nil {
		t.Errorf("the shell initialization should not fail: %v", err)
	}

	if _, ok := a.(*ShellAction); !ok {
		t.Error("the struct should be of type 'ShellAction'")
	}
}

func TestFactoryWithoutType(t *testing.T) {
	raw := RawAction{
		"without": "type",
	}
	_, err := Factory(raw)
	if err == nil {
		t.Error("the creation should not succeed without type")
	}
}

func TestFactoryWithUnknownType(t *testing.T) {
	raw := RawAction{
		"type": "unknown type",
	}
	_, err := Factory(raw)
	if err == nil {
		t.Error("the creation should not succeed with an unknown typename")
	}
}
