package actions

import (
	"deployer/display"
	"testing"
)

func createRawGit(repo, dst, name string) RawAction {
	return RawAction{
		rawKeyName:      name,
		rawKeyType:      GitType,
		rawGitRepoURL:   repo,
		rawGitDstFolder: dst,
	}
}

func TestGitInitializationSuccess(t *testing.T) {
	repo := "ssh://git@gitlab.com/J.pilleux/deployer.git"
	dst := "/tmp/deployer"
	name := "Git Folder"
	raw := createRawGit(repo, dst, name)
	a, err := NewGit(raw)

	if err != nil {
		t.Errorf("The git action initialization should be valid")
	}

	if a.name != name {
		t.Errorf(display.ExpectedMessage("name", name, a.name))
	}

	if a.destinationFolder != dst {
		t.Errorf(display.ExpectedMessage("destination folder", dst, a.destinationFolder))
	}

}

func TestGitInitializationFail(t *testing.T) {
	raw := RawAction{
		"nil": "nil",
	}
	_, err := NewGit(raw)
	if err == nil {
		t.Errorf("The git action should not be created")
	}
}

func TestGitValidateSSHRepository(t *testing.T) {
	repo := "ssh://git@gitlab.com/J.pilleux/deployer.git"
	err := validateGitRepo(repo)
	if err != nil {
		t.Errorf("The repo '%s' should be a valid git repository", repo)
	}
}

func TestGitValidateHTTPRepository(t *testing.T) {
	repo := "https://git@gitlab.com/J.pilleux/deployer.git"
	err := validateGitRepo(repo)
	if err != nil {
		t.Errorf("The repo '%s' should be a valid git repository", repo)
	}
}

func TestValidateNotValidRepository(t *testing.T) {
	repo := "not valid"
	err := validateGitRepo(repo)
	if err == nil {
		t.Errorf("The repo '%s' should not be a valid git repository", repo)
	}
}
