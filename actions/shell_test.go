package actions

import (
	"deployer/display"
	"testing"
)

func createRawShell(cmd, name string) RawAction {
	return RawAction{
		rawKeyName:         name,
		rawKeyType:         ShellType,
		rawShellKeyCommand: cmd,
	}
}

func TestShellInitializationSuccess(t *testing.T) {
	cmd := "ls -la"
	name := "List folders"
	raw := createRawShell(cmd, name)
	a, err := NewShell(raw)

	if err != nil {
		t.Errorf("the shell action initialization should be valid: %v", err)
	}

	if a.name != name {
		t.Errorf(display.ExpectedMessage("name", name, a.name))
	}

	if a.command != cmd {
		t.Errorf(display.ExpectedMessage("command", cmd, a.command))
	}
}

func TestShellInitializationFailure(t *testing.T) {
	raw := RawAction{
		"nil": "nil",
	}
	_, err := NewShell(raw)
	if err == nil {
		t.Errorf("The shell action should not be created")
	}
}
