package actions

import "fmt"

type RawAction map[string]string

const (
	rawKeyName = "name"
	rawKeyType = "type"
)

func (a RawAction) CheckKeyAreIn(actionType string, keys []string) error {
	missingKeys := make([]string, 0)
	for _, key := range keys {
		if _, ok := a[key]; !ok {
			missingKeys = append(missingKeys, key)
		}
	}

	// We found some missing keys
	if len(missingKeys) > 0 {
		return fmt.Errorf("the following keys are mandatory for type '%s': %v", actionType, missingKeys)
	}

	return nil
}

func (a RawAction) GetDefault(key, dft string) string {
	if val, ok := a[key]; ok {
		return val
	}
	return dft
}

func (a RawAction) GetDefaultName(defaultName string) string {
	return a.GetDefault(rawKeyName, defaultName)
}

func CreateActionsFromRaw(data []RawAction) ([]Actioner, error) {
	var actionList []Actioner
	for _, actionData := range data {
		action, err := Factory(actionData)
		if err != nil {
			return nil, err
		}
		actionList = append(actionList, action)
	}
	return actionList, nil
}
