package actions

import "fmt"

var CreateActions = map[string]interface{}{
	CopyType:  NewCopy,
	GitType:   NewGit,
	ShellType: NewShell,
}

func Factory(data RawAction) (Actioner, error) {
	actionType, ok := data["type"]
	if !ok {
		return nil, fmt.Errorf("the raw action does not have a type: %v", data)
	}

	funcPtr, ok := CreateActions[actionType]
	if !ok {
		return nil, fmt.Errorf("the action type '%s' does not exists", actionType)
	}

	var action Actioner
	var err error

	switch actionType {
	case CopyType:
		action, err = funcPtr.(func(RawAction) (*CopyAction, error))(data)
	case GitType:
		action, err = funcPtr.(func(RawAction) (*GitAction, error))(data)
	case ShellType:
		action, err = funcPtr.(func(RawAction) (*ShellAction, error))(data)
	default:
		err = fmt.Errorf("the action type '%s' does not exists, aborting", actionType)
	}

	if err != nil {
		return nil, err
	}

	return action, nil
}
