package actions

import (
	"deployer/connection"
	"deployer/display"
	"fmt"

	"github.com/elliotchance/orderedmap/v2"
)

type ShellAction struct {
	Action

	command string
}

const (
	ShellType string = "shell"

	rawShellKeyCommand string = "command"
)

func (action *ShellAction) Exec(conf connection.SSHConfig) (string, error) {
	sshClient, err := connection.CreateSSHClient(conf)
	if err != nil {
		return "", err
	}

	command := connection.NewCommand(action.command)
	commandOutput, err := sshClient.RunCommand(command)
	if err != nil {
		return "", err
	}
	successMessage := display.SuccessMessage(fmt.Sprintf("Shell command '%s' ran without errors", action.command))
	if len(commandOutput) <= 0 {
		return successMessage, nil
	}
	return fmt.Sprintf("%s\n\n%s", commandOutput, successMessage), nil
}

func (action *ShellAction) String() string {
	orderedMap := orderedmap.NewOrderedMap[string, string]()
	orderedMap.Set("Type", ShellType)
	orderedMap.Set("Name", action.name)
	orderedMap.Set("Command", action.command)
	return display.TabEachLine(orderedMap)
}

func NewShell(data RawAction) (*ShellAction, error) {
	mandatoryKeys := []string{rawShellKeyCommand}

	err := data.CheckKeyAreIn(ShellType, mandatoryKeys)
	if err != nil {
		return nil, err
	}

	a := &ShellAction{
		command: data[rawShellKeyCommand],
		Action:  *New(data.GetDefaultName("Shell Action")),
	}

	return a, nil
}

func (action *ShellAction) Name() string {
	return action.name
}
