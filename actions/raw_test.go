package actions

import "testing"

func TestCheckKeysAreIn(t *testing.T) {
	key := "test_key"
	raw := RawAction{
		key: "",
	}
	err := raw.CheckKeyAreIn("Test raw", []string{key})
	if err != nil {
		t.Errorf("the key '%s' should be detect present in '%v'", key, raw)
	}
}

func TestCheckKeyNotIn(t *testing.T) {
	key := "absent"
	raw := RawAction{
		"One":   "Two",
		"Three": "Four",
	}
	err := raw.CheckKeyAreIn("Test raw", []string{key})
	if err == nil {
		t.Errorf("the key '%s' should not be present in '%v'", key, raw)
	}
}

func TestGetDefault(t *testing.T) {
	key := "absent"
	dft := "default"
	raw := RawAction{
		"raw": "action",
	}

	actual := raw.GetDefault(key, dft)
	if actual != dft {
		t.Errorf("the key '%s' should not be found and '%s' should be returned", key, dft)
	}
}

func testActions() []RawAction {
	return []RawAction{
		createRawCopy("/tmp", "/dest", "Copy"),
		createRawGit("ssh://git@gitlab.com", "/tmp", "Git"),
		createRawShell("ls -la", "Shell"),
	}
}

func TestCreateActionsFromRaw(t *testing.T) {
	rawActions := testActions()
	actions, err := CreateActionsFromRaw(rawActions)
	if err != nil {
		t.Errorf("the action initialization should be valid: %v", err)
	}

	if len(actions) != len(rawActions) {
		t.Errorf("the number of action is not equal to tho number of raw actions: Expected=%d, Got=%d", len(rawActions), len(actions))
	}
}
