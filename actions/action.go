package actions

import (
	"deployer/connection"
	"deployer/display"
	"fmt"
	"sync"
)

type Actioner interface {
	Exec(connection.SSHConfig) (string, error)
	String() string
	Name() string
}

type Action struct {
	name string
}

func New(name string) *Action {
	return &Action{
		name: name,
	}
}

func (a *Action) Name() string {
	return a.name
}

func RunActionExec(action Actioner, waitGroup *sync.WaitGroup, conf connection.SSHConfig) {
	defer waitGroup.Done()
	actionResult, err := action.Exec(conf)
	actionName := action.Name()

	var toPrint string
	if err != nil {
		toPrint = display.FormatSection(actionName, display.FormatError("Error happened", err))
	} else {
		toPrint = display.FormatSection(actionName, actionResult)
	}

	fmt.Printf("%s\n\n", toPrint)
}
