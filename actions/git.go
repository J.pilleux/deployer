package actions

import (
	"deployer/connection"
	"deployer/display"
	"fmt"
	"strings"

	"github.com/elliotchance/orderedmap/v2"
)

// Needs the path to the directory to pull
const GitPullCommand = "git -C %s pull"

func formatPullCommand(repositoryPath string) string {
	return fmt.Sprintf(GitPullCommand, repositoryPath)
}

// Needs a repo then a directory
const GitCloneCommand = "git clone %s %s"

func formatCloneCommand(repositoryPath string, repositoryURL string) string {
	return fmt.Sprintf(GitCloneCommand, repositoryURL, repositoryPath)
}

type GitAction struct {
	Action

	repositoryURL     string
	destinationFolder string
}

const (
	GitType string = "git"

	rawGitRepoURL   string = "repositoryURL"
	rawGitDstFolder string = "destinationFolder"
)

func (action *GitAction) Exec(conf connection.SSHConfig) (string, error) {
	client, err := connection.CreateSSHClient(conf)
	if err != nil {
		return "", err
	}

	var (
		result         string
		successMessage string
		commandOutput  string
	)

	if !client.CheckExists(action.destinationFolder, connection.Folder) {
		result = fmt.Sprintf("Destination folder '%s' not found, cloning the repo '%s'", action.destinationFolder, action.repositoryURL)
		commandOutput, err = action.cloneRepo(client)
		if err != nil {
			return "", err
		}
		successMessage = display.SuccessMessage(fmt.Sprintf("Git repository '%s' successfully cloned in '%s'", action.repositoryURL, action.destinationFolder))
	} else {
		result = fmt.Sprintf("Destination folder '%s' already exists, updating it", action.destinationFolder)
		commandOutput, err = action.updateRepo(client)
		if err != nil {
			return "", err
		}
		successMessage = display.SuccessMessage(fmt.Sprintf("Git repository '%s' successfully cloned in '%s'", action.repositoryURL, action.destinationFolder))
	}

	result = fmt.Sprintf("%s\n\n%s\n\n%s", result, commandOutput, successMessage)
	return result, nil
}

func (action *GitAction) cloneRepo(client *connection.SSHClient) (string, error) {
	sshCommand := connection.NewCommand(formatCloneCommand(action.repositoryURL, action.destinationFolder))
	commandOutput, err := client.RunCommand(sshCommand)
	if err != nil {
		return "", fmt.Errorf("repository clone failed :\n%s", err.Error())
	}
	return commandOutput, nil
}

func (action *GitAction) updateRepo(client *connection.SSHClient) (string, error) {
	sshCommand := connection.NewCommand(formatPullCommand(action.destinationFolder))
	commandOutput, err := client.RunCommand(sshCommand)
	if err != nil {
		return "", fmt.Errorf("repository updating failed:\n%s", err.Error())
	}
	return commandOutput, nil
}

func (action *GitAction) String() string {
	orderedMap := orderedmap.NewOrderedMap[string, string]()
	orderedMap.Set("Type", GitType)
	orderedMap.Set("Name", action.name)
	orderedMap.Set("Repository URL", action.repositoryURL)
	orderedMap.Set("Destination Folder", action.destinationFolder)
	return display.TabEachLine(orderedMap)
}

const (
	sshProtocol  string = "ssh://"
	httpProtocol string = "https://"
)

func validateGitRepo(repo string) error {
	if !strings.HasPrefix(repo, sshProtocol) && !strings.HasPrefix(repo, httpProtocol) {
		return fmt.Errorf("the URI '%s' is not using HTTPS nor SSH protocol", repo)
	}
	return nil
}

func NewGit(data RawAction) (*GitAction, error) {
	mandatoryKeys := []string{rawGitRepoURL, rawGitDstFolder}

	err := data.CheckKeyAreIn(GitType, mandatoryKeys)
	if err != nil {
		return nil, err
	}

	repo := data[rawGitRepoURL]

	a := &GitAction{
		repositoryURL:     repo,
		destinationFolder: data[rawGitDstFolder],
		Action:            *New(data.GetDefaultName("Copy Action")),
	}

	return a, nil
}
