package main

import (
	action "deployer/actions"
	"deployer/cli"
	"deployer/config"
	"fmt"
	"os"
	"sync"
)

func main() {
	conf, err := config.GetConfig()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error occurred while getting configuration: %v\n", err)
	}

	var waitGroup sync.WaitGroup

	for _, currentAction := range conf.GetActions() {
		cli.PrintIfVerbose(fmt.Sprintf("Running action\n %s", currentAction.String()))

		waitGroup.Add(1)
		go action.RunActionExec(currentAction, &waitGroup, conf.GetSSHConfig())
	}

	waitGroup.Wait()
}
